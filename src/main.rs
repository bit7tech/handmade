use anyhow::Result;
use pixels::{Pixels, PixelsBuilder, SurfaceTexture};
use pixels_u32::*;
use winit::{
    dpi::PhysicalSize,
    event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

struct GlobalState {
    texture_size: PhysicalSize<u32>,
    window_size: PhysicalSize<u32>,
    xoffset: u32,
    pixels: Pixels,
}

impl GlobalState {
    fn new(window_width: u32, window_height: u32, scale: u32, window: &Window) -> Result<Self> {
        let surface = SurfaceTexture::new(window_width, window_height, window);
        let pixels = PixelsBuilder::new(window_width / scale, window_height / scale, surface)
            .enable_vsync(true)
            .build()?;

        Ok(Self {
            texture_size: PhysicalSize::<u32> {
                width: window_width / scale,
                height: window_height / scale,
            },
            window_size: PhysicalSize::<u32> {
                width: window_width,
                height: window_height,
            },
            xoffset: 0,
            pixels,
        })
    }

    fn get_scale(&self) -> u32 {
        self.window_size.width / self.texture_size.width
    }
}

fn main() -> Result<()> {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("Handmade Hero")
        .build(&event_loop)?;
    let mut global_state = GlobalState::new(800, 600, 1, &window)?;

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;
        match event {
            Event::WindowEvent { event, window_id } if window.id() == window_id => match event {
                WindowEvent::CloseRequested
                | WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            ..
                        },
                    ..
                } => *control_flow = ControlFlow::Exit,
                WindowEvent::Resized(new_size) => {
                    resize_window(&mut global_state, new_size);
                }
                WindowEvent::ScaleFactorChanged {
                    new_inner_size: new_size,
                    ..
                } => {
                    resize_window(&mut global_state, *new_size);
                }
                WindowEvent::Focused(_gained) => {}
                _ => {}
            },
            Event::RedrawRequested(window_id) if window_id == window.id() => {
                render_window(&mut global_state);
                let _ = global_state.pixels.render();
            }
            Event::MainEventsCleared => {
                update(&mut global_state);
                window.request_redraw();
            }
            _ => {}
        }
    });
}

fn resize_window(global_state: &mut GlobalState, size: PhysicalSize<u32>) {
    let scale = global_state.get_scale();

    let PhysicalSize::<u32> {
        width: window_width,
        height: window_height,
    } = size;
    let texture_width = window_width / scale;
    let texture_height = window_height / scale;

    // Resize the off-screen buffer.
    global_state
        .pixels
        .resize_buffer(texture_width, texture_height);
    // Resize the rendering area in the window.
    global_state
        .pixels
        .resize_surface(window_width, window_height);

    global_state.window_size = size;
    global_state.texture_size = PhysicalSize::<u32>::new(texture_width, texture_height);
}

fn render_window(global_state: &mut GlobalState) {
    let scale = global_state.get_scale();
    let frame = global_state.pixels.get_frame_u32();

    render_weird_gradient(
        frame,
        global_state.texture_size.width,
        scale,
        global_state.xoffset,
        0,
    );
}

fn render_weird_gradient(frame: &mut [u32], width: u32, scale: u32, xoffset: u32, yoffset: u32) {
    frame
        .chunks_mut(width as usize)
        .enumerate()
        .for_each(|(y, row)| {
            row.iter_mut().enumerate().for_each(|(x, pixel)| {
                *pixel = 0xff000000
                    + (((x as u32 + xoffset) * scale % 256) << 16)
                    + (((y as u32 + yoffset) * scale % 256) << 8);
            })
        });
}

fn update(global_state: &mut GlobalState) {
    global_state.xoffset += 1;
}
